#pragma once
#include <stdbool.h>
#include <stddef.h>

const char* combineStrings(const char *msg, size_t size){
    int i;
    char* temp;
    size_t counter = 1;
    temp = malloc(size*5+1);
    temp[0] = '\0'; 

    for (i = 0; i < size; ++i)
    {
        if(msg[i] == '&')
        {
            strcat(temp, "&amp;");
            counter += 5;
        }
        else if(msg[i] == '>')
        {
            strcat(temp,"&gt;");
            counter += 4;
        }
        else if(msg[i] == '<')
        {
            strcat(temp,"&lt;");
            counter += 4;
        }
        else
        {
            strncat(temp,&msg[i],1);
            counter += 1; 
        }

    }

    temp = realloc(temp, counter);
    return temp;
}