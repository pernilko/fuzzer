#include "utility.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
  const char *test = "hei&der<borte>!";
  size_t size = strlen(test);
  const char *res = combineStrings(test, size);
  printf("%s\n", res);
  free(res);
  return 0;
}