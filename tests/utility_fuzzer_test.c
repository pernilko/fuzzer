#include "utility.h"
#include <stddef.h>
#include <stdint.h>

int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
  free(combineStrings((const char *)data, size));
  return 0;
}